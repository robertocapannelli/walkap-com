<?php

namespace app\Controllers;

use Sober\Controller\Controller;

class ArchiveProject extends Controller
{
    public static function getProjectURL($post_id) {
        if (!class_exists('ACF')) return;

        return get_field('field_674b45378a840', $post_id);
    }

    public static function getProjectDuration($post_id) {
        if (!class_exists('ACF')) return;
        if (!$duration_group = get_field('field_674b45518a841', $post_id)) return;
        $duration = '';

        if ($duration_group['starting_date']) {
            $duration .= $duration_group['starting_date'];
        } else {
            return null;
        }

        if ($duration_group['ending_date']) {
            $duration .= ' - ' . $duration_group['ending_date'];
        } else {
            $duration .= ' - Presente';
        }

        return $duration;
    }

    public static function getProjectContributors($post_id) {
        if (!class_exists('ACF')) return;

        return get_field('field_674b46398a844', $post_id);
    }

    public static function getProjectCollaborations($post_id) {
        if (!class_exists('ACF')) return;

        return get_field('field_674b48488a847', $post_id);
    }

    public static function getProjectKind($post_id) {
        if (!class_exists('ACF')) return;

        return get_field('field_67713da1ddd0e', $post_id);
    }

    public static function getProjectLogo($post_id) {
        if (!class_exists('ACF')) return;

        return get_field('field_677155064e3b5', $post_id);
    }
}
