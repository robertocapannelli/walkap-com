<?php


namespace App\Controllers;


use App\Controllers\Partials\Book;
use Sober\Controller\Controller;

class SingleBook extends Controller {
    use Book;

    public function getRelatedBooks() {
        $terms = wp_get_post_terms( get_the_ID(), 'book-genre', [ 'fields' => 'ids' ] );

        if ( empty( $terms ) && is_wp_error( $terms ) ) {
            return;
        }

        $args = [
            'post_type'      => 'book',
            'post_status'    => 'publish',
            'posts_per_page' => 6,
            'post__not_in'   => [ get_the_ID() ],
            'orderby'        => 'rand',
            'tax_query'      => [
                [
                    'taxonomy' => 'book-genre',
                    'field'    => 'id',
                    'terms'    => $terms
                ]
            ]
        ];

        return new \WP_Query( $args );
    }
}
