<?php

namespace App\Controllers;

use App\Controllers\Partials\Book;
use App\Controllers\Partials\Books;
use Sober\Controller\Controller;

class FrontPage extends Controller {
    use Books;
    use Book;
}
