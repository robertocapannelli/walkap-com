<?php


namespace App\Controllers\Partials;


trait Book {
    public static function getAuthor() {
        if ( !class_exists( 'ACF' ) ) {
            return;
        }

        return get_field( 'author' );
    }

    public static function getPublicationYear() {
        if ( !class_exists( 'ACF' ) ) {
            return;
        }

        return get_field( 'published' );
    }

    public static function getAmazonReferralTitle() {
        if ( !class_exists( 'ACF' ) ) {
            return;
        }

        return get_field( 'amazon_referral_title' );
    }

    public static function getAmazonReferral() {
        if ( !class_exists( 'ACF' ) ) {
            return;
        }

        return get_field( 'amazon_referral' );
    }

    public static function getAmazonReferralTitleIta() {
        if ( !class_exists( 'ACF' ) ) {
            return;
        }

        return get_field( 'amazon_referral_title_ita' );
    }

    public static function getAmazonReferralIta() {
        if ( !class_exists( 'ACF' ) ) {
            return;
        }

        return get_field( 'amazon_referral_ita' );
    }

    public function getArchiveDescription() {
        if ( !class_exists( 'ACF' ) ) {
            return;
        }

        return get_field( 'archive_description', 'option' );
    }
}
