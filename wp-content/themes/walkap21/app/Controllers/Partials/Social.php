<?php


namespace App\Controllers\Partials;


trait Social {
    public function getSocialLinks(){
        if(!class_exists('ACF')){
            return;
        }

        return get_field('links', 'option');
    }
}
