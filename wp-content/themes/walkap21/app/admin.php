<?php

namespace App;

use App\Controllers\App;

/**
 * Theme customizer
 */
add_action('customize_register', function (\WP_Customize_Manager $wp_customize) {
    // Add postMessage support
    $wp_customize->get_setting('blogname')->transport = 'postMessage';
    $wp_customize->selective_refresh->add_partial('blogname', [
        'selector' => '.brand',
        'render_callback' => function () {
            bloginfo('name');
        }
    ]);
});

/**
 * Customizer JS
 */
add_action('customize_preview_init', function () {
    wp_enqueue_script('sage/customizer.js', asset_path('scripts/customizer.js'), ['customize-preview'], null, true);
});

add_action('wp_head', function () {
    if (is_user_logged_in() == 0 && wp_get_environment_type() !== 'local') :
        ?>
        <script id="mcjs">!function (c, h, i, m, p) {
                m = c.createElement(h), p = c.getElementsByTagName(h)[0], m.async = 1, m.src = i, p.parentNode.insertBefore(m, p)
            }(document, "script", "https://chimpstatic.com/mcjs-connected/js/users/15542629e66ca95cc5d79d2c6/975381e872d304cf0c654a678.js");</script>
    <?php
    endif;
});


function site_delete_transients()
{
    delete_transient('latest_posts');
    delete_transient('posts_wordpress_tag');
    delete_transient('sticky_posts');
    delete_transient('latest_books');
    delete_transient('latest_posts_sidebar');
}

add_action('delete_post', __NAMESPACE__ . '\\site_delete_transients');
add_action('edit_post', __NAMESPACE__ . '\\site_delete_transients');
add_action('post_updated', __NAMESPACE__ . '\\site_delete_transients');
add_action('save_post', __NAMESPACE__ . '\\site_delete_transients');

add_action('breadcrumbs', function () {
    if (function_exists('yoast_breadcrumb')) :
        yoast_breadcrumb('<p id="breadcrumbs">', '</p>');
    endif;
});


function project_archive_sorting($query){
    if(!is_admin() && $query->is_main_query() && is_post_type_archive('project')){
        $query->set('meta_key', 'project_duration_starting_date');
        $query->set('orderby', 'meta_value');
        $query->set('order', 'DESC');
    }
}
add_action('pre_get_posts', __NAMESPACE__ . '\\project_archive_sorting');
