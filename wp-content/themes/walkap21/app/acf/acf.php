<?php
if ( function_exists( 'acf_add_options_page' ) ) {
    $parent = acf_add_options_page( array(
        'page_title' => __('Theme Settings', 'sage'),
        'menu_title' => __('Theme Settings','sage'),
        'menu_slug'  => 'theme-general-settings',
        'redirect'   => false
    ) );
}
