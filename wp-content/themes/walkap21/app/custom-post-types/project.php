<?php

// Register Custom Post Type
function project_post_type() {

    $labels = array(
        'name'                  => _x( 'Progetti', 'Post Type General Name', 'sage' ),
        'singular_name'         => _x( 'Progetto', 'Post Type Singular Name', 'sage' ),
        'menu_name'             => __( 'Progetti', 'sage' ),
        'name_admin_bar'        => __( 'Progetto', 'sage' ),
        'archives'              => __( 'Archivio Progetti', 'sage' ),
        'attributes'            => __( 'Attributi Progetto', 'sage' ),
        'parent_item_colon'     => __( 'Progetto Genitore:', 'sage' ),
        'all_items'             => __( 'Tutti i Progetti', 'sage' ),
        'add_new_item'          => __( 'Aggiungi Nuovo Progetto', 'sage' ),
        'add_new'               => __( 'Aggiungi Nuovo', 'sage' ),
        'new_item'              => __( 'Nuovo Progetto', 'sage' ),
        'edit_item'             => __( 'Modifica Progetto', 'sage' ),
        'update_item'           => __( 'Aggiorna Progetto', 'sage' ),
        'view_item'             => __( 'Vedi Progetto', 'sage' ),
        'view_items'            => __( 'Vedi Progetti', 'sage' ),
        'search_items'          => __( 'Cerca Progetto', 'sage' ),
        'not_found'             => __( 'Non Trovato', 'sage' ),
        'not_found_in_trash'    => __( 'Non trovato nel cestino', 'sage' ),
        'featured_image'        => __( 'Immagine in evidenza', 'sage' ),
        'set_featured_image'    => __( 'Imposta immagine in evidenza', 'sage' ),
        'remove_featured_image' => __( 'Rimuovi immagine in evidenza', 'sage' ),
        'use_featured_image'    => __( 'Utilizza come immagine in evidenza', 'sage' ),
        'insert_into_item'      => __( 'Inserisci', 'sage' ),
        'uploaded_to_this_item' => __( 'Carica nel Progetto', 'sage' ),
        'items_list'            => __( 'Lista Progetti', 'sage' ),
        'items_list_navigation' => __( 'Navigazione lista Progetti', 'sage' ),
        'filter_items_list'     => __( 'Filtra lista Progetti', 'sage' ),
    );
    $rewrite = array(
        'slug'                  => 'progetto',
        'with_front'            => true,
        'pages'                 => true,
        'feeds'                 => false,
    );
    $args = array(
        'label'                 => __( 'Progetto', 'sage' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'page-attributes' ),
        'taxonomies'            => array('project_tag'),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-portfolio',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'show_in_rest'          => true,
        'can_export'            => true,
        'has_archive'           => 'progetti',
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'rewrite'               => $rewrite,
        'capability_type'       => 'page',
    );
    register_post_type( 'project', $args );

}
add_action( 'init', 'project_post_type', 0 );
