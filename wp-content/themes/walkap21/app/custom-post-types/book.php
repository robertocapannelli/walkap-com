<?php
// Register Custom Post Type
function books_post_type() {

    $labels = array( 'name'                  => _x( 'Libri', 'Post Type General Name', 'sage' ),
                     'singular_name'         => _x( 'Libro', 'Post Type Singular Name', 'sage' ),
                     'menu_name'             => __( 'Libri', 'sage' ),
                     'name_admin_bar'        => __( 'Libro', 'sage' ),
                     'archives'              => __( 'Libro Archives', 'sage' ),
                     'attributes'            => __( 'Libro Attributes', 'sage' ),
                     'parent_item_colon'     => __( 'Libro genitore:', 'sage' ),
                     'all_items'             => __( 'Tutti i Libri', 'sage' ),
                     'add_new_item'          => __( 'Aggiungi nuovo Libro', 'sage' ),
                     'add_new'               => __( 'Aggiungi nuovo', 'sage' ),
                     'new_item'              => __( 'Nuovo Libro', 'sage' ),
                     'edit_item'             => __( 'Modifica Libro', 'sage' ),
                     'update_item'           => __( 'Aggiorna Libro', 'sage' ),
                     'view_item'             => __( 'Vedi Libro', 'sage' ),
                     'view_items'            => __( 'Vedi Libri', 'sage' ),
                     'search_items'          => __( 'Cerca Libro', 'sage' ),
                     'not_found'             => __( 'Non trovato', 'sage' ),
                     'not_found_in_trash'    => __( 'Non trovato nel cestino', 'sage' ),
                     'featured_image'        => __( 'Immagine in evidenza', 'sage' ),
                     'set_featured_image'    => __( 'Imposta immagine in evidenza', 'sage' ),
                     'remove_featured_image' => __( 'Rimuovi Immagine in evidenza', 'sage' ),
                     'use_featured_image'    => __( 'Usa come immagine in evidenza', 'sage' ),
                     'insert_into_item'      => __( 'Insert into Book', 'sage' ),
                     'uploaded_to_this_item' => __( 'Caricate in questo Libro', 'sage' ),
                     'items_list'            => __( 'Lista Libri', 'sage' ),
                     'items_list_navigation' => __( 'Navigazione della lista dei Libri', 'sage' ),
                     'filter_items_list'     => __( 'Filtra lista Libri', 'sage' ), );

    $rewrite = array( 'slug'       => 'libro',
                      'with_front' => true,
                      'pages'      => true,
                      'feeds'      => true, );


    $args = array( 'label'               => __( 'Libro', 'sage' ),
                   'description'         => __( 'Descrizione Libro', 'sage' ),
                   'labels'              => $labels,
                   'supports'            => array( 'title',
                                                   'editor',
                                                   'thumbnail',
                                                   'custom-fields',
                                                   'excerpt' ),
                   'taxonomies'          => array( 'book-genre' ),
                   'hierarchical'        => false,
                   'public'              => true,
                   'show_ui'             => true,
                   'show_in_menu'        => true,
                   'menu_position'       => 5,
                   'show_in_admin_bar'   => true,
                   'show_in_nav_menus'   => true,
                   'can_export'          => true,
                   'has_archive'         => 'libri',
                   'exclude_from_search' => false,
                   'publicly_queryable'  => true,
                   'rewrite'             => $rewrite,
                   'capability_type'     => 'page',
                   'show_in_rest'        => true, );
    register_post_type( 'book', $args );

}

add_action( 'init', 'books_post_type', 0 );
