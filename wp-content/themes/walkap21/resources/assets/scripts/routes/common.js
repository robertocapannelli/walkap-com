import Swiper, {Autoplay} from 'swiper';
import {getMmenuJs} from '../util/mmenu';
import {getAnchorScroll} from '../util/anchorScroll';
import {getArticlesSlider, getBookSlider, getRelatedSlider} from '../util/sliders';

Swiper.use([Autoplay]);

export default {
  init() {
    // JavaScript to be fired on all pages
    getMmenuJs();
    getAnchorScroll();
    getArticlesSlider();
    getBookSlider();
    getRelatedSlider();
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
