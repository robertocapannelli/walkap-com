<article @php post_class() @endphp>
  <header class="mb-4">
    @include('partials/entry-meta')
    @include('partials/post-tags')
    <h1 class="entry-title">{!! get_the_title() !!}</h1>
  </header>
  <div class="entry-content">
    @php the_content() @endphp
    <hr class="border-primary mb-4">
    <div class="pb-5">
      <p>
        Se ti è piaciuto questo articolo o ti è stato utile in qualche modo, lascia un commento, condividilo con i
        tuoi amici e seguimi sui social.
      </p>
      @include('partials.single-post.social-links')
      @include('partials.single-post.referral-links')
    </div>
  </div>

  <footer>
    {!! wp_link_pages(['echo' => 1, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
  </footer>
  @include('partials.post.related-posts')
  @php comments_template('/partials/comments.blade.php') @endphp
</article>


