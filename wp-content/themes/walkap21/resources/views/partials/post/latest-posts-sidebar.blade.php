@if($get_latest_posts_sidebar->have_posts())
  <section id="sidebar-latest-posts" class="mb-5">
    <h3 class="mb-3">Ultimi articoli</h3>
    <ul class="list-unstyled">
      @while($get_latest_posts_sidebar->have_posts())
        @php $get_latest_posts_sidebar->the_post(); @endphp
        <li class="mb-2 px-3 py-2 border-primary border d-flex align-items-center justify-content-start rounded">
          @if(has_post_thumbnail())
            <figure class="mr-2 mb-0" style="min-width: 40px;">
              {!! get_the_post_thumbnail(null, 'sidebar_latest_articles', ['class' => 'w-100 h-auto', 'style' => 'max-width: 50px']) !!}
            </figure>
          @endif
          <a href="{{get_permalink()}}">
            {!! get_the_title() !!}
          </a>
        </li>
      @endwhile
    </ul>
  </section>
  @php wp_reset_postdata() @endphp
@endif
