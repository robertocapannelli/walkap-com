@if($get_wordpress_posts->have_posts())
  <section class="mb-4">
    <h2>WordPress</h2>
    <div class="swiper-container articles-slider mb-4" id="articles-slider">
      <div class="swiper-wrapper">
        @while($get_wordpress_posts->have_posts())
          @php $get_wordpress_posts->the_post() @endphp
          <div class="swiper-slide h-auto">
            <article @php post_class('mb-4 border-primary border p-3 h-100 rounded') @endphp>
              <header>
                @include('partials/entry-meta')
                @include('partials.post-tags')
                <h2 class="entry-title">
                  <a href="{{ get_permalink() }}">
                    {!! get_the_title() !!}
                  </a>
                </h2>
              </header>
              <div class="entry-summary">
                @php the_excerpt() @endphp
              </div>
            </article>
          </div>
        @endwhile
      </div>
    </div>
    @php wp_reset_postdata() @endphp

    <a href="{{get_term_link('wordpress', 'post_tag')}}" class="btn btn-primary">Vedi altri articoli su WordPress <i class="bi bi-arrow-up-right"></i></a>
  </section>
@endif

