@if($get_social_links)
  <section class="social-links">
    <h5 class="h4">Seguimi sui social</h5>

    <ul class="list-unstyled">
      @foreach($get_social_links as $link)
        <li>
          {{$link['name']}}: <a href="{{$link['url']}}" target="_blank" class="text-referral">@<?=$link['username']?></a>
        </li>
      @endforeach
    </ul>
  </section>
@endif
