<article @php post_class('mb-4 col-sm-6 col-md-4 col-lg-3') @endphp itemscope="" itemtype="https://schema.org/Book">
  @if(has_post_thumbnail())

    @php $alt_attribue = get_the_title() . ' - ' . (ArchiveBook::getAuthor() ?? '') @endphp
    <a href="{{get_permalink()}}">
      <figure class="mb-0 border-top border-left border-right border-primary rounded-top">
        {!! get_the_post_thumbnail(null, 'book_thumbnail', ['class'=>'img-fluid w-100', 'itemprop' => 'image', 'alt' => $alt_attribue]) !!}
      </figure>
    </a>
  @endif
  <div class="border-primary border p-3 rounded-bottom">
    <header>
      <h2 class="entry-title h5 mb-0" itemprop="name">
        <a href="{{get_permalink()}}">
          {!! get_the_title() !!}
        </a>
      </h2>
      <h4 class="h6 text-muted" itemprop="author" itemscope itemtype="https://schema.org/Person">
        <small>{{ArchiveBook::getAuthor() ?? ''}}</small>
      </h4>
    </header>
    <div class="entry-summary" style="font-size: 14px">
      @php the_excerpt() @endphp
    </div>
    <footer class="d-flex align-items-center justify-content-between">
      <div>
        @if(get_field('currently_reading'))
          <small class="bg-fluo text-primary rounded font-weight-bold p-1 border-primary border">
            In lettura
          </small>
        @endif
      </div>
      <small class="text-muted text-right">{{ArchiveBook::getPublicationYear()}}</small>
    </footer>


  </div>
</article>
